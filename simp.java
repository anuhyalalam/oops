class Greetings {
  String name;
  public void greet() {
    System.out.println("hello");
  }
}
class Intro extends Greetings {
  public void display() {
    System.out.println("My name is " + name);
  }
}
class Main {
  public static void main(String[] args) {
    Intro details = new Intro();
    details.greet();
    details.name = "anu";
    details.display();
  }
}

