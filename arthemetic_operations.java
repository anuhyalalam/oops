import java.util.*;
import java.lang.*;

class Main {
  public static void main(String[] args) {
    int x;
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter A And B Values:");
    int a = sc.nextInt();
    int b = sc.nextInt();
    System.out.println(
        "Enter Which Arithmetic Operation\n1)Addition\n2)Subtraction\n3)Multiplication\n4)Division\n5)Remainder\n");
    int c = sc.nextInt();
    if (c == 1) {
      x=a+b;
      System.out.println("Addition Of Two Numbers Are: "+x);
    } else if (c == 2) {
      x=a-b;
      System.out.println("Subtraction Of Two Numbers Are: "+x);
    } else if (c == 3) {
      x=a*b;
      System.out.println("Multiplication Of Two Numbers Are: "+x);
    } else if (c == 4) {
      x=a/b;
      System.out.println("Division Of Two Numbers Are: "+x);
    } else {
      x=a%b;
      System.out.println("Addition Of Two Numbers Are: "+x);
    }
  }
}