#include<iostream>
using namespace std;

class Student
{
                private:
                    string name,college_name;
                    int roll,code;
                    double sem;

                public:
                    Student ()
                    {
                        cout<<"\nStudent Details constructor: "<<endl;
                    }

void read()
{
        cout<<"\nEnter the student Name : ";
        cin>>name;
        cout<<"\nEnter the student college name : ";
        cin>>college_name;
        cout<<"\nEnter the student roll number : ";
        cin>>roll;
        cout<<"\nEnter the college code : ";
        cin>>code;
        cout<<"\nEnter the student semester percentage : ";
        cin>>sem;
}

void disp()
{
    cout<<"\nStudent Details are : \n";
        cout<<"\nStudent Name : "<<name<<endl;
        cout<<"\nCollege name   is : "<<college_name<<endl;
        cout<<"\nRoll number is : "<<roll<<endl;
        cout<<"\nCollege code is : "<<code<<endl;
        cout<<"\nSemester percentage : "<<sem<<endl;
}

~Student()
{
        cout<<"\nThank you";
}
};

int main()
{
        Student obj;
        obj.read ();
        obj.disp ();

    return 0;
}