
import java.awt.*;
import java.awt.event.*;
public class UI implements ActionListener{
    UI(){
         Frame fr=new Frame();
         fr.setTitle("LOGIN PAGE");
         fr.setBackground(Color.lightGray);
         fr.setBounds(0,0,500,500);
         fr.setLayout(null);
         TextField message=new TextField();
         message.setBounds(170,290,150,20);
         Label title=new Label("LOGIN FORM");
         title.setBounds(200,50,100,20);
         Label unameLabel=new Label("User name");
         unameLabel.setBounds(50,100,100,20);
         TextField unameBox=new TextField();
         unameBox.setBounds(170,100,100,20);
         Label passLabel=new Label("password");
         passLabel.setBounds(50,150,100,20);
         TextField passBox=new TextField();
         passBox.setBounds(170,150,100,20);
         Button submit=new Button("LOGIN");
         submit.setBounds(200,250,80,20);
         
         fr.add(title);
         fr.add(unameLabel);
         fr.add(unameBox);
         fr.add(passLabel);
         fr.add(passBox);
         fr.add(submit);
         fr.setVisible(true);
         fr.add(message);
         fr.addWindowListener(
            new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    fr.dispose();
                }
            }
         );
         fr.addMouseListener(
            new MouseAdapter() {
                public void mouseClicked(MouseEvent e){
                    fr.dispose();
                }
            }
         );
          submit.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    message.setText("Submitted Successfully");
                }
            }
          ); 
    }
    public static void main(String[] args){
        UI obj=new UI();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
    }
}
