#include<iostream>
using namespace std;
class GrandMother   
{
  public:
  GrandMother()
  {
      cout << "GrandMother() is called..." << endl;
  }
};
class Father : public GrandMother 
{
    public:
    Father()
    {
     cout << "Father() inherits from GrandMother()..." << endl;   
    }
};
class Mother 
{
    public:
    Mother()
    {
        cout << "Mother() is called..." << endl;
    }
};
class Son : public Father,public Mother   
{};
class Daughter : public Father
{
    public:
    Daughter()
    {
        cout << "Daughter() inherits from Father()..." << endl;
    }
};
class Brother : public Mother{}; 
class Sister : public Mother{};  
class GrandFather  
{
  public:
  GrandFather()
  {
      cout << "GrandFather() is called..." << endl;
  }
};
class GrandDaughter : public Father, public GrandFather{}; 
class Baby : public GrandDaughter{};  
int main()
{
    cout << "single inheritence" << endl;
   Father obj;
    cout << "\nmultiple inheritence" << endl;
    Son obj1;
    cout << "\nmultilevel inheritence" << endl;
    Daughter obj2;
    cout << "\nHierarchial inheritence" << endl;
    Brother obj3;
    Sister obj4;
    cout << "\nHybrid inheritence" << endl;
    GrandDaughter obj5;
    Baby obj6;
    return 0;
}

