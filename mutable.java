public class MutableImmutableDemo {
public static void main(String[] args) {
StringBuilder sb = new StringBuilder("Hello");
System.out.println("Original StringBuilder: " + sb);
sb.append(" World");
System.out.println("Modified StringBuilder: " + sb);
String str1 = "123";
System.out.println("Original String: " + str1);
String str2 = str1.concat(" World");
System.out.println("Modified String: " + str2);
}
}
