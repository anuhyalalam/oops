import java.util.*;
import java.lang.*;
class Main {
  public static void main(String[] args) {
    evenodd();
  }
  static void evenodd()
  {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter A Number To Check Whether The Number Is Even Or Odd");
    int a=sc.nextInt();
    if(a%2==0)
    {
      System.out.println("The Given Number "+a+" Is An Even Number");
    }
    else{
      System.out.println("The Given Number "+a+" Is An Odd Number");
    }
  }
}