public class AccessSpecifierDemo{
    
    private int priVar;      // private member variable
    protected int proVar;   // protected member variable
    public int pubVar;      // public member variable
    
    // method to set the values of member variables
    public void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
    
    // method to print the values of member variables
    public void getVar() {
        System.out.println("Private Variable: " + priVar);
        System.out.println("Protected Variable: " + proVar);
        System.out.println("Public Variable: " + pubVar);
    }
    
    public static void main(String[] args) {
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(04, 25, 34);          obj.getVar();             
    }
}


