#include<iostream>
using namespace std;
class Box{
    public:
    float area,volume;
    float length,width,height;

    void setValues(float length1, float width1,float height1){
        length = length1;
        width=width1;
        height=height1;
    }
    void boxArea(float length, float width,float height){
        area=2*(length*width+length*height+width*height);
        cout<<"Area of box is "<<area<<endl;
     
    }
    void boxVolume(float length, float width, float height);
    friend void displayBoxDimensions(Box);
    inline void displayWelcomeMessage();

};
void Box::boxVolume(float length, float width, float height){
    volume=(length*width*height);
    cout<<"Volume of box is "<<volume <<endl;
}
void displayBoxDimensions(Box b){
        cout<<"Box length is "<<b.length<<endl;
        cout<<"Box width is "<<b.width<<endl;
        cout<<"Box height is"<<b.height<<endl;
        

    }

inline void Box::displayWelcomeMessage(){
    cout<<"Welcome to the program"<<endl;
}
int main(){
    float l,w,h;
    Box b;
    cout<<"Enter length of the box "<<endl;
    cin>>l;
    cout<<"Enter width of the box "<<endl;
    cin>>w;
    cout<<"Enter height of the box "<<endl;
    cin>>h;
    Box gift;
    gift.boxArea(l,w,h);
    gift.boxVolume(l,w,h);
    gift.setValues(l,w,h);
    displayBoxDimensions(gift);
    gift.displayWelcomeMessage();
}