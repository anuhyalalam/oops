#include <iostream>
using namespace std;
class Person {
  public:
    string name;
    int age;

    void introduce(){
        cout << "Hi, my name is " << name << " and I am " << age << " years old." << endl;
    }
};
class Student : public Person {
  public:
    string major;

    void study(){
        cout << name << " is studying " << major << "." << endl;
    }
};
int main() {
    Student john;
    john.name = "anuhya";
    john.age = 18;
    john.major = "Computer Science";
    john.introduce();
    john.study();
    return 0;
}

