interface Person{
void display();
}
class Student implements Person{
public void display() {
System.out.println("This is display method of the Student class");
}
}
class Lecturer implements Person{
public void display() {
System.out.println("This is display method of the teacher class");
}
}
public class AbstractionExample{
public static void main(String args[]) {
Person person1 = new Student();
person1.display();
Person person2 = new Lecturer();
person2.display();
}
}
