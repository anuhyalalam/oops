    class inherit {
    public void show() {
        System.out.println("Student inheriting properties from Person");
    }
}

class children extends inherit { // single
    public void show5() {
        System.out.println("SINGLE INHERITANCE");
    }
}

// multilevel
class Students extends inherit {
    public void show1() {
        System.out.println("I am a Student who belongs to Person class");
    }
}

class EngineeringStudent extends Students {
    public void show2() {
        System.out.println("Engineering Student inheriting properties from Student");
    }
}

class MedicalStudent extends inherit {
    public void show3() {
        System.out.println("HIERARCHICAL");
    }
}

class MbaStudents extends inherit {
    public void show4() {
        System.out.println("HIERARCHICAL-1");
    }
}

class C {
    public void disp() {
        System.out.println("C");
    }
}

class A extends C {
    public void disp1() {
        System.out.println("A");
    }
}

class B extends C {
    public void disp2() {
        System.out.println("B");
    }
}

class D extends A {
    public void disp3() {
        System.out.println("D");
    }
}

class inheritance {

    public static void main(String args[]) {
        children obj = new children();
        EngineeringStudent obj1 = new EngineeringStudent();
        MbaStudents obj2 = new MbaStudents();
        D obj3 = new D();
        System.out.println("SINGLE INHERITANCE");
        obj.show();
        obj.show5();
        System.out.println("MULTILEVEL INHERITANCE");
        obj1.show();
        obj1.show1();
        obj1.show2();
        System.out.println("HIERARCHICAL INHERITANCE");
        obj2.show();
        obj2.show4();
        System.out.println("HYBRID INHERITANCE");
        obj3.disp1();
        obj3.disp3();
    }
}
