#include <iostream>
using namespace std;
class Animal {
public:
    void eat() {
        cout << "Animal is eating" << endl;
    }
    void sleep() {
        cout << "Animal is sleeping" << endl;
    }
    void move() {
        cout << "Animal is moving" << endl;
    }
private:
    void breath(){
        cout << "Animal is breathing" << endl;
    }
};
class DogPrivate : private Animal {
public:
    void bark() {
        cout << "Dog is barking" << endl;
    }
    void doDogThings() {
        eat();
        sleep();
        move();
        bark();
    }
};
class DogProtected : protected Animal {
public:
    void bark() {
        cout << "Dog is barking" << endl;
    }
    void doDogThings() {
        eat();
        sleep();
        move();
        bark();
    }
};
class DogPublic : public Animal {
public:
    void bark() {
        cout << "Dog is barking" << endl;
    }

    void doDogThings() {
        eat();
        sleep();
        move();
        bark();
    }
};
int main() {
    DogPrivate privateDog;
    DogProtected protectedDog;
    DogPublic publicDog;

    cout << "Testing DogPrivate class:" << endl;
    privateDog.doDogThings(); 

    cout << "Testing DogProtected class:" << endl;
    protectedDog.doDogThings();

    cout << "Testing DogPublic class:" << endl;
    publicDog.doDogThings(); 

    return 0;
}
