class PPP {
    public int x = 10;
    protected int y = 20;
    // private int z = 30;
}

class A extends PPP {
    public void display() {
        System.out.println(x);
    }

    public void display1() {
        System.out.println(y);
    }

    public void display2() {
        // System.out.println(z);
    }
}

class B extends PPP {
    protected void show() {
        System.out.println(x);
    }

    protected void show1() {
        System.out.println(y);
    }

    protected void show2() {
        // System.out.println(z);
    }
}

class C extends PPP {
    private void show3() {
        System.out.println(x);
    }

    private void show4() {
        System.out.println(y);
    }

    // private void show5() {
    // System.out.println(z);

    public static void main(String[] args) {
        C obj = new C();
        obj.show3();
        obj.show4();
        // obj.show5();
    }
}



