 class Student{
    String fullName;
    int rollNum;
    double semPercentage;
    String collegeName;
    int collegeCode;

    Student(String fullName,int rollNum, double semPercentage, String collegeName, int collegeCode){
        this.fullName=fullName;
        this.rollNum=rollNum;
        this.semPercentage=semPercentage;
        this.collegeName=collegeName;
        this.collegeCode=collegeCode;

    }
    public static void main(String[] args) {
        System.out.println("Constructor is called");
        Student s = new Student("xyz", 24, 80.55, "abc college", 4567);
        System.out.println("FullName is: "+ s.fullName);
        System.out.println("RollNumber is: "+ s.rollNum);
        System.out.println("Sem percentage is: "+ s.semPercentage);
        System.out.println("College Name is: "+ s.collegeName);
        System.out.println("College Code is: "+ s.collegeCode);
         
        s = null;
        System.gc();
    }
    
    public void finalize(){
        System.out.println("Student object is destroyed by the Garbage Collector");
        }

}
