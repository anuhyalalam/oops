import java.util.Scanner;
class Interface implements Runnable{
    public void multiply(int num){
        for (int i=0;i<=12;i++){
            System.out.println(num + "*" + i + "=" + num * i);
        }
    }
    public void run(){
        System.out.println("Enter a number:");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();
        multiply(num);
    }
    public static void main(String[] args){
        Interface obj = new Interface();
        Thread obj1 = new Thread(obj); 
        obj1.start();
    }
}