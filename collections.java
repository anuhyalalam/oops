import java.util.*;
public class JavaCollectionsExample {
 public static void main(String[] args) {
 // List example (ArrayList)
 List<String> names = new ArrayList<>();
 names.add("Alice");
 names.add("Bob");
 names.add("Charlie");
 System.out.println("List example:");
 for (String name : names) {
 System.out.println(name);
 }
 // Set example (HashSet)
 Set<Integer> numbers = new HashSet<>();
 numbers.add(1);
 numbers.add(2);
 numbers.add(3);
 numbers.add(2); // Duplicate value, will be ignored
 System.out.println("\nSet example:");
 for (int number : numbers) {
 System.out.println(number);
 }
 // Queue example (LinkedList)
 Queue<String> queue = new LinkedList<>();
 queue.offer("Apple");
 queue.offer("Banana");
 queue.offer("Orange");
 System.out.println("\nQueue example:");
 while (!queue.isEmpty()) {
 String fruit = queue.poll();
 System.out.println(fruit);
 }
 // Map example (HashMap)
 Map<String, Integer> scores = new HashMap<>();
 scores.put("Alice", 90);
 scores.put("Bob", 85);
 scores.put("Charlie", 95);
 System.out.println("\nMap example:");
 for (Map.Entry<String, Integer> entry : scores.entrySet()) {
 String name = entry.getKey();
 int score = entry.getValue();
 System.out.println(name + ": " + score);
 }
 }
}
