public class Student2 {
    String fullName;
    int rollNum;
    double semPercentage;
    String collegeName;
    int collegeCode
    public Student2() {
        this.collegeName = "MVGR";
        this.collegeCode = 33;
    }
    public Student2(String fullName, double semPercentage) {
        this.fullName = fullName;
        this.semPercentage = semPercentage;
        this.collegeName = "MVGR";
        this.collegeCode = 33;
    }
    protected void finalize() {
        System.out.println("Student object is destroyed.");
    }

    public static void main(String[] args) {
        Student2 s1 = new Student2();
        System.out.println("Default Constructor values");
        System.out.println("College Name: " + s1.collegeName);
        System.out.println("College Code: " + s1.collegeCode);
        Student2 s2 = new Student2("Anu", 85);
        System.out.println("Parameterized Constructor values");
        System.out.println("Full Name: " + s2.fullName);
        System.out.println("Sem Percentage: " + s2.semPercentage);
        s1 = null;
        s2 = null;
        System.gc();
    }
}
