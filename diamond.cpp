#include <iostream>
using namespace std;
class Animal {
public:
    void eat() { cout << "Animal::eat()" << endl; }
};
class Mammal : virtual public Animal { 
public:
    void breathe() { cout << "Mammal::breathe()" << endl; }
};
class Bird : virtual public Animal { 
public:
    void fly() { cout << "Bird::fly()" << endl; }
};
class Bat : public Mammal, public Bird {
public:
    void feedBaby() { cout << "Bat::feedBaby()" << endl; }
};
int main() {
    Bat b;
    b.eat(); 
    b.breathe();
    b.fly();
    b.feedBaby();
    return 0;
}
