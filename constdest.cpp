#include<iostream>
using namespace std;
class Student{
    private:
    string college_name;
    int college_code;
    public:
    Student()
    {
        cout<<"Default constructor: "<<endl;
        college_name = "MVGR";
        college_code = 33;
    }
    void display() {
        cout<<"College name is: "<<college_name<<endl;
        cout<<"College code is: "<<college_code<<endl;
    }
    ~Student(){
        cout<<"Thank you"<<endl;
    }
};

class student{
    private:
    string fullName;
    double semPercentage;

    public:
    student(string student_name)
    {
        //cout<<"FullName is :"<<endl;
        fullName = student_name;
        semPercentage = 80;
    }

    void display()
    {
        cout<<"Full name of the student is :"<<fullName<<endl;
        cout<<"Semester percentage of the student is :"<<semPercentage<<endl;
    }
    ~student(){
        cout<<"Welcome"<<endl;
    }
};

int main() 
{
    Student obj;
    obj.display();

    student obj1("Anuhya");
    obj1.display();
    
    return 0;