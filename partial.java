abstract class Shape {
protected int x;
protected int y;
public Shape(int x, int y) {
this.x = x;
this.y = y;
}
public void display(){
System.out.println("X:" + x +" and Y:" + y);
}
public abstract double calculateArea();
}
class Circle extends Shape {
private double radius;
public Circle(int x, int y, double radius) {
super(x, y);
this.radius = radius;
}
public double calculateArea() {
return Math.PI * radius * radius;
}
}
class Rectangle extends Shape {
private int width;
private int height;
public Rectangle(int x, int y, int width, int height) {
super(x, y);
this.width = width;
this.height = height;
}
public double calculateArea() {
return width * height;
}
}
public class ImpureAbsExample {
public static void main(String[] args) {
Shape shape1 = new Circle(10, 20, 5);
Shape shape2 = new Rectangle(30, 40, 8, 12);
double area1 = shape1.calculateArea();
double area2 = shape2.calculateArea();
shape1.display();
shape2.display();
System.out.println("Area of shape1: " + area1);
System.out.println("Area of shape2: " + area2);
}
}
