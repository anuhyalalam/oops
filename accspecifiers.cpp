#include<iostream>
using namespace std;
class AccessSpecifierDemo
{
   private:
   int priVar;
   protected:
   int proVar;
   public:
   int pubVar;
   public:
   void setVar(int priValue,int proValue, int pubValue)
   {
    priVar=priValue;
    proVar=proValue;
    pubVar=pubValue;
   }
   void getVar()
   {
    cout<<"Private: "<<priVar<<endl;
    cout<<"Protected: "<<proVar<<endl;
    cout<<"Public: "<<pubVar<<endl;
   }
};
int main()
{
    AccessSpecifierDemo obj;
    int pri,pro,pub;
    cout<<"Enter private,protected,public values:"<<endl;
    cin>>pri>>pro>>pub;
    obj.setVar(pri,pro,pub);
    obj.getVar();


}
